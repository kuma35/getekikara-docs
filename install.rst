.. getekikara documentation master file, created by
   sphinx-quickstart on Sun Sep 16 15:27:49 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

============
インストール
============

Pythonのインストール
--------------------

Python2.Xを利用します。

Windows
.......

1. http://www.python.org/ftp/python/2.7.2/python-2.7.2.msi (32bit版)

   http://www.python.org/

   Pythonの公式サイトより2.7.X系(または2系でそれ以上)をインストールします。

2. コマンドプロンプトでPythonが動くことを確認します。

   python --version 

Linux
.....

お使いのディストリビューション提供のパッケージか、 http://www.python.org/ から持ってきたものを
インストールしてください。コンソールで動作確認してください。

BeautifulSoupインストール
-------------------------

BeautifulSoup 3(3.1.2)を使います。
http://www.crummy.com/software/BeautifulSoup/

3.2.1をインストール(2012/10/02現在) 
....................................

  http://www.crummy.com/software/BeautifulSoup/download/3.x/BeautifulSoup-3.2.1.tar.gz

ダウンロード後、展開(.tar.gzを解凍できるツールが必要です。作者はたまたまLhaplusを使っています)
展開したフォルダでコマンドプロンプトに入り、 

  python setu.py install

インストール後は python BeautifulSoupTests.py を動かして OK を確認します。

.. note::

   BeautifulSoup 4は使い方が大幅に変わっているためまだ対応できていません。3を使ってください。

getekikara展開
--------------

getekikaraのファイルを展開。
展開したまま使ってもよし、フォルダごと移動しても構いません。

実行
----

Windows
.......

WindowsはgetEkikara.bat.hoge を getEkikara.bat に名前を変更してください。

実行してみる。

書式
  getEkikara.bat 『適当な名前(拡張子抜き)』 『下り時刻表の1ページ目のURL』 『上り時刻表の1ページ目のURL』

例(［ＪＲ］只見線 平日)
  getekikara.bat tadami http://www.ekikara.jp/newdata/line/1301601/down1_1.htm http://www.ekikara.jp/newdata/line/13061601/up1_1.htm

→tadami.sqlite(中間ファイル)とtadami.oudを生成する

.. note::

  中間ファイルは削除して構いません。

Linux
.....

 Linuxの場合は、getEkikara.shを使います。
 なお、端末の文字コードがUTF-8だったりした場合も、oudファイルは変換無しに読めるよう、cp932, CRLFで出力します。
