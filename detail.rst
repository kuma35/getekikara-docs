
====
詳細
====

gettimetable.py
---------------

書式
  python gettimetable.py -d DBFILE [options] [URL [ URL... ]]

機能
  えきから時刻表をスクレイピングしてsqliteファイルに書き出します。

必須引数
........

-d DBFILE, --db-file=DBFILE  出力するsqliteファイル名を指定します。

                             なお、既に存在するファイルを指定した場合は追加処理を行います。
                             途中止めした場合でも再度同じ指定で実行することにより処理を続行できます。

URL
  えきから時刻表の指定の路線の路線時刻表のページを指定します。
  指定のURLのページから何ページあるか読み取り、全ページを取得するので、
  1つだけ指定すればOKです。
  
  上下、平日休日は自動でに読み取らないので、別々に取り込んでください。
  
  但し、同じ路線の同じ日(上り平日なら下り平日といった具合)は同時に指定するとoudファイル作成時に一度に生成できます。

オプション
..........

-s, --status                 ステータスメッセージをコンソールに出力します。
                             指定しないとエラー以外画面に出さないので、指定をおすすめします。

--version                    show program's version number and exit
-h, --help                   show this help message and exit
-p URL, --proxy=URL          プロキシを通す場合に設定します。

                               --proxy=hogehoge.com:8080
                             
.. -n, --no-separate-train      If having multiple train numbers, separate train
                                timetable by train number.

以下は指定したURLのページを読むときのリトライ間隔、最大試行回数を指定します。
通常は指定する必要はありません。地下鉄やトンネルが多い区間等、通信が途切れやすい場合に
指定します。

--timeout=SEC                set timeout. max wait is SEC x COUNT
--retry=COUNT                set retry count max. max wait is SEC x COUNT

setdiadata.py
-------------

gettimetaable.pyで出力したsqliteファイルを取り込み、OuDiaのデータファイル(\*.oud)を生成します。

書式
  python  setdiadata.py -d DBFILE -f FILENAME [options] 

必須引数
........

-d DBFILE, --db-file=DBFILE        読み取るsqliteファイル名を指定します。

-f OUTFILE, --output-file=OUTFILE  出力するファイルの名前(拡張子除く)を指定します。

                                   tadami.oudを生成する場合は以下のように指定します。
                                   
                                     -f tadami

                                   別オプションで出力形式を指定しますが、出力形式の指定を省略した場合はoudファイルなので
                                   拡張子oudが付加されます。

.. note::

  -f , --output-file オプションは既存のファイルがあると黙って上書きします。


オプション
..........

-s, --status                                  print status messages.
--version                                     show program's version number and exit
-h, --help                                    show this help message and exit
-o TYPE, --output-format=TYPE                 oud or csv

.. note::

  csvは現在メンテしていません。

.. -t TIMETABLE-ID, --timetable-id=TIMETABLE-ID  target TIMETABLE-ID. default is 0.
                                                 Listing timetable-id by
                                                 infotimetable.py -l  --station-list=FILE
                                                 specify station list FIlE
.. --station-list-encode=ENCODE                  file ENCODE for station list. default is CP932
.. --no-check-order                              no checking train stop time order. default checking order
