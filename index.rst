.. getekikara documentation master file, created by
   sphinx-quickstart on Sun Sep 16 15:27:49 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to getekikara's documentation!
======================================

Contents:

.. toctree::
   :maxdepth: 2
   
   overview.rst
   synopsis.rst
   install.rst
   detail.rst
   testing.rst


