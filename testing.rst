
======
テスト
======

単体テスト
----------

py.test を実行

疎通確認
--------

疎通確認として以下を実行し、結果をOuDia上で目検しています。

.. note::
  URLはドキュメント記述時のものです。

.. note::
  Windowsの場合は ./getEkikara.sh を getEkikara.bat と読み替えて下さい。


東海道新幹線(東京-新大阪)上下平日
.................................

特に東京～新横浜辺りのこだまをひかり・のぞみが抜き去る辺り。
完全とはいかないが、新横浜の着時刻を取得できることでダイヤグラム上の矛盾がある程度解決できているのを確認。

./getEkikara.sh sample-toukaidou-shinkansen-day-updown http://www.ekikara.jp/newdata/line/2301011/down1_1.htm http://www.ekikara.jp/newdata/line/2301011/up1_1.htm


只見線上下平日
..............

時刻表上では主要駅扱いでなくても列車交換が比較的綺麗に描き出せているはず。
また、時刻表の途中駅始発の場合に始発駅の発時刻取得が抜けるバグが再発していないかチェック。

./getEkikara.sh sample-tadami-day-updown http://www.ekikara.jp/newdata/line/1301601/down1_1.htm http://www.ekikara.jp/newdata/line/1301601/up1_1.htm


鹿児島本線上り平日(八千代～下関)
................................

列車番号が同一で途中で種別が変わるものを分割できるか確認する(1006Mなど)。

./getEkikara.sh sample-kagoshima-yachiyo-shimonoseki-day-up http://www.ekikara.jp/newdata/line/4001011/down1_1.htm http://www.ekikara.jp/newdata/line/4001011/up1_1.htm


[名古屋市営]名城線(ナゴヤドーム前矢田～ナゴヤドーム前矢田)平日
..............................................................

ループ線。

./getEkikara.sh sample-nagoyashiei-meijyou-day http://www.ekikara.jp/newdata/line/2306021/down1_1.htm http://www.ekikara.jp/newdata/line/2306021/up1_1.htm

.. warning::
  主要駅の検出に失敗することがある(着時刻発時刻は取得できている)。そのため、
  検出できていない主要駅が終点となる列車の着時刻が「||」と表示されることがある。
  →お手数ですがOuDia上で駅の種別を変更または着時刻を表示するようにしてください。


[伊予鉄道]１系統(環状線)(松山市駅前～松山市駅前)平日
....................................................

ループ線。

./getEkikara.sh sample-iyotetsu-1-day http://www.ekikara.jp/newdata/line/3801041/down1_1.htm


飯田線上下平日
..............

豊橋～天竜峡、天竜峡～辰野を統合したデータを生成(URL4本同時指定)。
線名が同じで時刻表が分割されている場合は一括指定したものを統合できる
(最初飯田線用に作り始めたためこの機能がある。他で動くかどうか未検証)。

./getEkikara.sh sample-iida-day-updown http://www.ekikara.jp/newdata/line/2301071/down1_1.htm http://www.ekikara.jp/newdata/line/2301071/up1_1.htm http://www.ekikara.jp/newdata/line/2301072/down1_1.htm http://www.ekikara.jp/newdata/line/2301072/up1_1.htm


ポートアイランド線平日
......................

./getEkikara.sh sample-portisland-from-koube http://www.ekikara.jp/newdata/line/2809011/down1_1.htm


