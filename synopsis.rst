.. getekikara documentation master file, created by
   sphinx-quickstart on Sun Sep 16 15:27:49 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

========
SYNOPSYS
========


書式(Windows)
  getEkikara.bat <<db/gen file body>> <<URL>> [ <<URL>> ... ]

書式(Linux)
  ./getEkikara.sh <<db/gen file body>> <<URL>> [ <<URL>> ... ]

<<db/gen file body>>
  中間ファイルとして使用するsqliteファイルと、生成するoudファイルのボディ部(拡張子を除いた部分)を指定します。

<<URL>>
  えきから時刻表の路線時刻表のページのURLを指定。上りと下り両方指定しても、片方でも構いません。
  ただし、生成するデータは最初に指定したURLの路線のものだけです(上下は同時に指定可能)。

例:
  ./getekikara.bat tadami http://www.ekikara.jp/newdata/line/1301601/down1_1.htm http://www.ekikara.jp/newdata/line/1301601/up1_1.htm 

tadami.sqlite
  中間ファイルとして生成します。既に同名のファイルが存在した場合は取得済みのデータをそのまま使います。一から取得しなおしたいときは
  一旦ファイルを消去してから実行してください。

tadami.oud
  生成するOuDia用のファイルです。既に存在する場合は無条件に上書きします。
